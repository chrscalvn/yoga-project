export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  mode: 'spa',
  head: {
    title: 'yohanes-gabriel',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
  },
  server: {
    port: 4001 // default: 3000
  },

  polyfill: {
    features: [
      /* 
          Feature without detect:

          Note: 
            This is not recommended for most polyfills
            because the polyfill will always be loaded, parsed and executed.
      */
      {
        require: 'url-polyfill' // NPM package or require path of file
      },

      /* 
          Feature with detect:

          Detection is better because the polyfill will not be 
          loaded, parsed and executed if it's not necessary.
      */
      {
        require: 'intersection-observer',
        detect: () => 'IntersectionObserver' in window,
      },

      /*
          Feature with detect & install:

          Some polyfills require a installation step
          Hence you could supply a install function which accepts the require result
      */
      {
        require: 'smoothscroll-polyfill',

        // Detection found in source: https://github.com/iamdustan/smoothscroll/blob/master/src/smoothscroll.js
        detect: () => 'scrollBehavior' in document.documentElement.style && window.__forceSmoothScrollPolyfill__ !== true,

        // Optional install function called client side after the package is required:
        install: (smoothscroll) => smoothscroll.polyfill()
      }
    ]
  },
  bootstrapVue: {
    bootstrapCSS: false, // Or `css: false`
    bootstrapVueCSS: true // Or `bvCSS: false`
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/css/custom.css',
    '~/assets/scss/style.scss',
    'animate.css/animate.min.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '@/plugins/validator.js', ssr: false }, // do not turn on validator on ssr mode
    { src: '@/plugins/axios.js'},
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/fontawesome',
  ],

  router: {
    middleware: ['auth'],
    linkActiveClass: 'active-link'
  },  

  auth: {
    localStorage: false,
    strategies: {
      local: {
        token: {
          property: 'profileToken',
          global: true,
          required: true,
          type: 'Bearer',
        },
        user: {
          property: false,
          autoFetch: false
        },
        endpoints: {
          login: { url: '/users/login', method: 'post', headers:{'Id': '6147f6ff87b8f825e06d77da', 'Secret':'0a1037ec-3860-4ace-a0db-f8e87d0bdce5', 'Authorization':'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c'} } ,
          user: { url:'/users/profile', method: 'get', headers:{'Id': '6147f6ff87b8f825e06d77da', 'Secret':'0a1037ec-3860-4ace-a0db-f8e87d0bdce5'}},
          reset_password: { url:'/users/reset-password', method: 'post', headers:{'Id': '6147f6ff87b8f825e06d77da', 'Secret':'0a1037ec-3860-4ace-a0db-f8e87d0bdce5'}},
          forgot_pass: {url:'/users/forgot-password', method: 'post', headers:{'Id': '6147f6ff87b8f825e06d77da', 'Secret':'0a1037ec-3860-4ace-a0db-f8e87d0bdce5'}}
        }
      }
    }
  },  

  axios: {
    baseURL: 'http://localhost:3005/yoga/', // Used as fallback if no runtime config is provided
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    ['bootstrap-vue/nuxt'],
    ['nuxt-polyfill', {
      ueI18nLoader: true 
    }],
    '@nuxtjs/i18n',
    '@nuxtjs/axios',
    '@nuxtjs/auth-next'
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: [ '@amcharts/amcharts4' ],
  }
}
