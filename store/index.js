import Vue from 'vue'
import Vuex from 'vuex'
import Setting from './Setting/index'
import Chat from './Chat/index'
Vue.use(Vuex)

// export const state = () => ({
//     answers: []
// })

// export const getters = () => ({
//     getAnswer(){
//         //
//     }
// })

// export const actions = ({state, payload}) => ({
//     addAnswer()
//     {
//         commit("ADD_ANSWER")
//     },

//     SubmitAnswer()
//     {
//         //
//     }
// })

// export const mutations = () => ({
//     ADD_ANSWER()
//     {
        
//     }
// })

export const store = new Vuex.Store({
    strict:true
})
  